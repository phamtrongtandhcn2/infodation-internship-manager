import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import { Router } from './src/routers/Router';

class App extends React.Component {
	render() {
		return <Router />;
	}
}

AppRegistry.registerComponent(appName, () => App);
