export default function shortTitle(c) {
	let a = '';
	if (c.length > 24) {
		for (let i = 0; i < 24; i++) {
			a = a + c[i];
		}
		a = a + '...';
	} else a = a + c;
	return a;
}
