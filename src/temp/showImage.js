import React, { Component } from 'react';
import { CameraRoll, Image, ScrollView, StyleSheet, Text, PermissionsAndroid } from 'react-native';

export default class Test extends Component {
	state = { photos: null };

	render() {
		let { photos } = this.state;
		return (
			<ScrollView style={styles.container}>
				{photos ? this._renderPhotos(photos) : <Text style={styles.paragraph}>Fetching photos...</Text>}
			</ScrollView>
		);
	}

	_renderPhotos(photos) {
		let images = [];
		for (let { node: photo } of photos.edges) {
			images.push(
				<Image
					source={photo.image}
					resizeMode="contain"
					style={{ height: 100, width: 100, resizeMode: 'contain' }}
				/>
			);
		}
		return images;
	}

	componentDidMount() {
		this._getPhotosAsync().catch(error => {
			console.error(error);
		});
	}

	async requestExternalStoreageRead() {
		try {
			const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, {
				title: 'Cool App ...',
				message: 'App needs access to external storage',
			});

			return granted == PermissionsAndroid.RESULTS.GRANTED;
		} catch (err) {
			//Handle this error
			return false;
		}
	}

	async _getPhotosAsync() {
		if (await this.requestExternalStoreageRead()) {
			let photos = await CameraRoll.getPhotos({ first: 4 });
			this.setState({ photos });
			// let photos = await CameraRoll.getPhotos({ first: 4 });
			// this.setState({ photos });
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#ecf0f1',
	},
	paragraph: {
		margin: 24,
		fontSize: 18,
		fontWeight: 'bold',
		textAlign: 'center',
		color: '#34495e',
	},
});
