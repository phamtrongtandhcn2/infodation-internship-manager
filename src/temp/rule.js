// Chung:
// - Xem thông tin tài khoản
// - Đổi mật khẩu

// Student:
// - Xem thông tin Đợt thực tập
// - Xem nội dung training program
// - Nộp bài tập của training program
// - Xem nội dung ProjectIntern và Project(nếu có)

// Manager:
// - Xem, tạo, cập nhật nội dung training program
// - Check bài tập trong training program để đánh giá "pass?"

// Admin:
// - Tạo, xem, cập nhật, xóa thông tin Internship
// - Tạo, xem, cập nhật, xóa thông tin ProjectIntern
// - Tạo, xem, cập nhật, xóa thông tin Project
// - Tạo, xem, cập nhật, xóa Role
// - Tạo, xem, cập nhật, xóa Function
// - Tạo, xem, cập nhật, xóa thông tin Student
// - Tạo, xem, cập nhật, xóa thông tin Manager