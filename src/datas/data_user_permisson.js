var Data_User_Permisson = [
	{},
	{
		UserPermissonID: 1,
		Username: 'admin',
		FunctionID: 1,
		Licensed: 1,
	},
	{
		UserPermissonID: 2,
		Username: 'user',
		FunctionID: 2,
		Licensed: 1,
	},
	{
		UserPermissonID: 3,
		Username: 'user',
		FunctionID: 0,
		Licensed: 0,
	},
];
export default Data_User_Permisson;
