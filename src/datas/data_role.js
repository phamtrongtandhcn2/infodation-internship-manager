var Data_Role = [
	{},
	{
		RoleID: 1,
		RoleName: 'Adminstrator',
	},
	{
		RoleID: 2,
		RoleName: 'Student',
	},
	{
		RoleID: 3,
		RoleName: 'Viewer',
	},
];
export default Data_Role;
