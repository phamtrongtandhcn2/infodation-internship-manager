var Data_Timeline = [
	{
		key:0,
		time: '20/03',
		title: 'Cài đặt môi trường',
		description:
			'Cài đặt các môi trường để hỗ trợ cho React Native và báo cáo: NodeJS, Android Studio, Genymotion, Git, Source Tree,..  ',
		icon: require('../images/tick.png'),
	},
	{
		key:1,
		time: '21/03',
		title: 'Tạo ứng dụng đầu tiên',
		description: 'Tìm hiểu và tạo ứng dụng react native đầu tiên ',
		icon: require('../images/tick.png'),
	},
	{
		key:2,
		time: '22/03',
		title: 'Tìm hiểu State, Props',
		description: 'Tìm hiểu và sử dụng được Props và State trong React native',
		icon: require('../images/tick.png'),
	},
	{
		key:3,
		time: '23/03',
		title: 'Tìm hiểu về Layout trong React Native',
		description: 'Tìm hiểu và sử dụng được Dimetion, Flex, JustifyContent,...',
		icon: require('../images/tick.png'),
	},
	{
		key:4,
		time: '24/03',
		title: 'Tìm hiểu các Component cơ bản (1)',
		description: 'Tìm hiểu và sử dụng được các component: Button, Text, View,..',
		icon: require('../images/cross.png'),
	},
];
export default Data_Timeline;
