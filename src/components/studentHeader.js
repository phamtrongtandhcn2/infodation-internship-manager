import React from 'react';
import { StyleSheet, Text,  View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
export default class DrawerLayout extends React.Component {
	render() {
		const {navigation} = this.props
		showDrawer =()=>navigation.openDrawer();
		return (
			<View style={styles.header}>
				<TouchableOpacity onPress = {showDrawer}>
					<Icon name="menu" color="white" size={35} />
				</TouchableOpacity>
				<Text style={styles.titleHeader}>{this.props.titleHeader}</Text>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	header: {
		height: 60,
		flexDirection: 'row',
		borderBottomColor: 'purple',
		borderBottomWidth: 2,
		alignItems: 'center',
		paddingHorizontal: 2,
		backgroundColor: '#993399',
	},
	titleHeader:{
		color: 'white',
		fontSize: 18,
		marginHorizontal: 5,
	}
});
