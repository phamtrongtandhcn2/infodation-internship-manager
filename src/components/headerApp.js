import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default class HeaderApp extends React.Component {
	render() {
		return (
			<View style={{ flex: 1, justifyContent: 'center' }}>
				<Text style={styles.headerTitle}>{this.props.headerTitle}</Text>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	headerTitle: {
		justifyContent: 'center',
		alignItems: 'center',
		fontSize: 18,
        marginLeft: 5,
        color: 'white'
	},
});
