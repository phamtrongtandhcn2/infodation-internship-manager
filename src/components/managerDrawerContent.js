import React from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, ScrollView, Alert, SafeAreaView } from 'react-native';
import { DrawerItems } from 'react-navigation';
import Data_Manager from '../datas/data_manager';
ManagerDrawerContent = props => {
	for (let manager of Data_Manager) {
		if (manager.managerID == global.loginName) {
			return (
				<SafeAreaView style={{ flex: 1 }}>
					<View style={styles.container}>
						<View style={styles.header}>
							<Image
								source={{
									uri: manager.avatar,
								}}
								style={{
									marginTop: 20,
									borderRadius: 100,
									width: 60,
									height: 60,
									borderColor: 'white',
									borderWidth: 2,
									alignItems: 'center',
								}}
							/>
							<Text style={styles.hello}>Xin chào, {manager.fullName}</Text>
							<Text style={styles.email}>{manager.email}</Text>
						</View>
						<View style={styles.body}>
							<ScrollView>
								<DrawerItems {...props} />
							</ScrollView>
						</View>
						<TouchableOpacity
							style={styles.logout}
							onPress={() => {
								Alert.alert(
									'Đăng xuất!',
									'Bạn muốn đăng xuất?',
									[
										{
											text: 'Hủy',
											onPress: () => {
												return null;
											},
										},
										{
											text: 'Đồng ý',
											onPress: () => {
												props.navigation.navigate('LoginRouter');
											},
										},
									],
									{ cancelable: false }
								);
							}}
						>
							<Text style={{ color: 'gray' }}>Đăng xuất</Text>
						</TouchableOpacity>
						<View style={styles.footer}>
							<Text style={{ color: 'white' }}>Copyright by INFODation</Text>
						</View>
					</View>
				</SafeAreaView>
			);
		}
	}
};
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		flex: 1 / 5,
		backgroundColor: '#993399',
		paddingHorizontal: 10,
		justifyContent: 'flex-end',
		paddingBottom: 5,
	},
	body: {
		flex: 15 / 20,
	},
	footer: {
		flex: 1 / 20,
		backgroundColor: 'gray',
		justifyContent: 'center',
		alignItems: 'center',
	},
	logout: {
		flex: 1 / 20,
		backgroundColor: 'whitesmoke',
		justifyContent: 'center',
		alignItems: 'center',
	},
	hello: {
		marginTop: 5,
		color: 'white',
		alignItems: 'center',
		fontSize: 14,
		fontWeight: 'bold',
	},
	email: {
		fontStyle: 'italic',
		color: 'whitesmoke',
		alignItems: 'center',
		fontSize: 12,
	},
	menu_button: {
		justifyContent: 'space-between',
		flexDirection: 'row',
		alignItems: 'center',
		// backgroundColor: '#DDDDDD',
		padding: 10,
		borderBottomWidth: 1,
		borderBottomColor: '#999999',
	},
	menu_title: {
		textAlign: 'right',
		color: '#993399',
		fontWeight: 'bold',
	},
});
export default ManagerDrawerContent;
