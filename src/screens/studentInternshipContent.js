import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Timeline from 'react-native-timeline-listview';
import Data_Timeline from '../datas/data_timeline';
import StudentHeader from '../components/studentHeader';
import { TouchableOpacity } from 'react-native-gesture-handler';
export default class StudentInternshipContent extends Component {
	constructor() {
		super();
		this.onEventPress = this.onEventPress.bind(this);
		this.data = Data_Timeline;
		this.state = {
			showInfomation: false,
		};
	}
	onEventPress = data => {
		try {
			global.timeline = data.key;
			this.props.navigation.navigate('StudentInternshipDetail');
		} catch {}
	};
	showInfomation = () => {
		this.setState({
			showInfomation: true,
		});
	};
	hideInfomation = () => {
		this.setState({
			showInfomation: false,
		});
	};
	render() {
		return (
			<View style={styles.container}>
				<StudentHeader titleHeader={'Internship'} navigation={this.props.navigation} />
				<View>
					{this.state.showInfomation === false ? (
						<TouchableOpacity style={styles.showInfomation} onPress={this.showInfomation}>
							<Text style={{ color: 'white' }}>Hiện thông tin</Text>
						</TouchableOpacity>
					) : (
						<TouchableOpacity onPress={this.hideInfomation} style={styles.hideInfomation}>
							<View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
								<Text style={styles.infoText}>Đợt thực tập:</Text>
								<Text style={[styles.infoText, { fontSize: 20 }]}> = 2019INFATCU =</Text>
							</View>
							<View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
								<Text style={styles.infoText}>Ngày bắt đầu: 01/03/2019</Text>
								<Text style={styles.infoText}>Ngày kết thúc: 31/05/2019</Text>
								<Text style={styles.infoText}>Người quản lý: Mr. Staoaw</Text>
								<Text style={styles.infoText}>Liên hệ: 0123456789</Text>
								<Text style={styles.infoText}>Email: support@infodation.nl</Text>
							</View>
							<View
								style={{
									marginTop: 10,
									justifyContent: 'center',
									alignItems: 'center',
								}}
							>
								<Text style={{ color: 'white' }}>{`=> Ẩn <=`} </Text>
							</View>
						</TouchableOpacity>
					)}
				</View>

				<Timeline
					style={styles.list}
					data={this.data}
					circleSize={20}
					circleColor="#F44336"
					lineColor="#993399"
					timeContainerStyle={{ minWidth: 50 }}
					timeStyle={{
						textAlign: 'center',
						backgroundColor: '#993399',
						color: 'white',
						padding: 5,
						borderRadius: 13,
					}}
					descriptionStyle={{
						color: 'gray',
						borderBottomWidth: 1,
						paddingBottom: 5,
						borderBottomColor: 'gray',
					}}
					innerCircle={'icon'}
					onEventPress={this.onEventPress}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	list: {
		flex: 1,
		justifyContent: 'center',
		padding: 10,
		backgroundColor: 'white',
	},
	showInfomation: {
		justifyContent: 'center',
		padding: 10,
		alignItems: 'center',
		padding: 5,
		backgroundColor: '#663366',
	},
	hideInfomation: {
		padding: 10,
		padding: 5,
		backgroundColor: '#663366',
	},
	infoText: { color: 'white', fontWeight: 'bold' },
});
