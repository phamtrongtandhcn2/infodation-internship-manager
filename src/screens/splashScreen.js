import React from 'react';
import { Image, View, ImageBackground, ActivityIndicator, StyleSheet, Animated } from 'react-native';
export default class SplashScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fadeValue: new Animated.Value(1),
		};
		setTimeout(() => {
			Animated.timing(this.state.fadeValue, {
				toValue: 0,
				duration: 200,
			}).start();
			this.props.navigation.navigate('LoginRouter');
		}, 2800);
	}
	render() {
		return (
			<Animated.View style={[Style.Rootcontainer, { opacity: this.state.fadeValue }]}>
				<ImageBackground source={_ImageBackground} style={Style.ImageBackground}>
					<View style={Style.LogoContainer}>
						<Image style={Style.ImageLogo} source={_ImageLogo} />
						<View>
							<ActivityIndicator size="large" color="#994599" />
						</View>
					</View>
				</ImageBackground>
			</Animated.View>
		);
	}
}
const _ImageLogo = require('../images/ifd_intern_logo.png');
const _ImageBackground = require('../images/backgroud_login.jpg');
const Style = StyleSheet.create({
	Rootcontainer: {
		flex: 1,
	},
	LogoContainer: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	ImageLogo: { width: 200, height: 200 },
	ImageBackground: { flex: 1 },
});
