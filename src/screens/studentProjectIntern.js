import React, { Component } from 'react';
import { Text, View } from 'react-native';
import StudentHeader from '../components/studentHeader';
import IconAns5 from 'react-native-vector-icons/FontAwesome5';
class StudentProjectIntern extends Component {
	static navigationOptions = {
		drawerLabel: 'Internship Project',
		drawerIcon: () => <IconAns5 name="file-archive" color="#993399" size={18} />,
	}
	render() {
		return (
			<View style={{flex:1}}>
				<StudentHeader titleHeader={'Internship Project'} navigation={this.props.navigation} />
				<Text>StudentProjectIntern</Text>
			</View>
		);
	}
}

export default StudentProjectIntern;
