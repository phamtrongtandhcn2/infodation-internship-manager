import React from 'react';
import {
	StyleSheet,
	TextInput,
	View,
	Text,
	ImageBackground,
	TouchableOpacity,
	BackHandler,
	Animated,
	Keyboard,
	TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Data_Login from '../datas/data_login';
export default class LoginScreen extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loginName: '15it002',
			loginPass: '1',
			springValue: new Animated.Value(1),
			avatar: 'http://file.vforum.vn/hinh/2018/03/hinh-anh-hinh-nen-zoro-dep-nhat-one-piece-4.jpg',
			studentID: '',
			fullName: '',
			sex: '',
			birthday: '',
			email: '',
			address: '',
			phoneNumber: '',
			unit: '',
			intro: '',
		};
	}
	componentDidMount() {
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
	}

	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}
	handleChangeLoginName = loginName => {
		this.setState({ loginName: loginName });
	};
	handleChangeLoginPass = loginPass => {
		this.setState({ loginPass: loginPass });
	};
	_keyboardDidShow = () => {
		Animated.spring(this.state.springValue, {
			toValue: 0.7,
			duration: 200,
		}).start();
	};
	_keyboardDidHide = () => {
		Animated.spring(this.state.springValue, {
			toValue: 1,
			duration: 200,
		}).start();
	};

	quit = () => {
		BackHandler.exitApp();
	};
	render() {
		const { navigate } = this.props.navigation;
		signup = () => {
			navigate('SignUpScreen');
		};
		checkLogin = () => {
			for (let login of Data_Login) {
				if (login.Username == this.state.loginName && login.Password == this.state.loginPass) {
					global.loginName = login.Username;
					if (login.RoleID == 1) {
						navigate('ManagerRouter');
					} else if (login.RoleID == 2) {
						navigate('StudentRouter');
					}
				}
			}
		};
		return (
			<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
				<View style={Style.RootContainer}>
					<ImageBackground source={_ImageBackground} style={Style.ImageBackground}>
						<View style={Style.LogoContainer}>
							<Animated.Image
								source={_ImageLogo}
								style={[
									Style.ImageLogo,
									{ transform: [{ scale: this.state.springValue }], alignSelf: 'center' },
								]}
							/>
						</View>
						{/* <KeyboardAvoidingView  style={Style.InputContainer} behavior="padding" enabled> */}
						<View style={Style.InputContainer}>
							<View style={Style.Input}>
								<Icon name="user" color="gray" size={14} />
								<TextInput
									placeholder="Tên đăng nhập"
									multiline={false}
									maxLength={20}
									returnKeyType="next"
									onChangeText={this.handleChangeLoginName}
									onSubmitEditing={() => this.refs.txtPassword.focus()}
									value={this.state.loginName}
									autoCorrect={false}
									keyboardType="visible-password"
								/>
							</View>
							<View style={Style.Input}>
								<Icon name="lock1" color="gray" size={14} />
								<TextInput
									secureTextEntry={true}
									maxLength={20}
									placeholder="Mật khẩu"
									multiline={false}
									onChangeText={this.handleChangeLoginPass}
									ref="txtPassword"
									value={this.state.loginPass}
								/>
							</View>
							<View>
								<TouchableOpacity style={Style.LoginButton} onPress={checkLogin}>
									<Text style={Style.WhiteText}>Đăng nhập</Text>
								</TouchableOpacity>
								<TouchableOpacity style={Style.QuitButton} onPress={this.quit}>
									<Text style={Style.QuitText}>Thoát</Text>
								</TouchableOpacity>
								<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
									<TouchableOpacity style={Style.OtherButton}>
										<Text>Quên mật khẩu</Text>
									</TouchableOpacity>
									<TouchableOpacity style={Style.OtherButton} onPress={signup}>
										<Text>Đăng ký</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>
						{/* </KeyboardAvoidingView> */}
					</ImageBackground>
				</View>
			</TouchableWithoutFeedback>
		);
	}
}
//Variable

const _ImageLogo = require('../images/ifd_intern_logo.png');
const _ImageBackground = require('../images/backgroud_login.jpg');
//Style
const Style = StyleSheet.create({
	RootContainer: {
		flex: 1,
	},
	LogoContainer: {
		marginTop: 20,
		flex: 1 / 3,
		height: 300,
		justifyContent: 'center',
		alignItems: 'center',
	},
	InputContainer: {
		flex: 2 / 3,
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: 10,
	},
	ImageBackground: {
		flex: 1,
	},
	ImageLogo: {
		width: 175,
		height: 175,
	},
	Input: {
		flexDirection: 'row',
		alignItems: 'center',
		borderColor: 'white',
		borderWidth: 1,
		paddingHorizontal: 20,
		marginVertical: 10,
		height: 40,
		width: 250,
		borderRadius: 20,
		backgroundColor: 'whitesmoke',
	},
	LoginButton: {
		backgroundColor: '#993399',
		height: 40,
		width: 250,
		borderRadius: 20,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 10,
	},
	QuitButton: {
		backgroundColor: 'dimgray',
		height: 40,
		width: 250,
		borderRadius: 20,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 10,
		marginTop: 5,
	},
	OtherButton: {
		marginBottom: 10,
		justifyContent: 'center',
		alignItems: 'center',
	},
	QuitText: {
		color: 'white',
		fontWeight: 'bold',
	},
	WhiteText: {
		color: 'white',
		fontWeight: 'bold',
	},
});
