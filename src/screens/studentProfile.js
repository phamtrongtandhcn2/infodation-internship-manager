import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet,
	Image,
	ScrollView,
	TouchableOpacity,
	TextInput,
	Picker,
	CameraRoll,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import StudentHeader from '../components/studentHeader';
import IconAns5 from 'react-native-vector-icons/FontAwesome5';
import Data_Student from '../datas/data_student';
// import Data_Provinces from '../datas/data_provinces';
import DatePicker from 'react-native-datepicker';
class StudentProfile extends Component {
	static navigationOptions = {
		drawerLabel: 'Profile',
		drawerIcon: () => <IconAns5 name="user-cog" color="#993399" size={18} />,
	};

	render() {
		return (
			<View style={styles.container}>
				<StudentHeader titleHeader={'Profile'} navigation={this.props.navigation} />
				<ProfileComponent />
			</View>
		);
	}
}
class ProfileComponent extends React.Component {
	constructor(props) {
		super(props);
		this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
		this.state = {
			avatar: 'http://file.vforum.vn/hinh/2018/03/hinh-anh-hinh-nen-zoro-dep-nhat-one-piece-4.jpg',
			studentID: '',
			fullName: '',
			sex: '',
			birthday: '',
			email: '',
			address: '',
			phoneNumber: '',
			unit: '',
			intro: '',
			update: false,
			changeAvatar: false,
			changeFullName: false,
			changeSex: false,
			changeBirthday: false,
			changeEmail: false,
			changeAddress: false,
			changePhoneNumber: false,
			changeUnit: false,
			changeIntro: false,
			date: '2016-05-15',
		};
	}
	selectPhotoTapped() {
		const options = {
			quality: 1.0,
			maxWidth: 500,
			maxHeight: 500,
			storageOptions: {
				skipBackup: true,
			},
		};

		ImagePicker.showImagePicker(options, response => {
			console.log('Response = ', response);
			if (response.didCancel) {
				console.log('User cancelled photo picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log('User tapped custom button: ', response.customButton);
			} else {
				// You can also display the image using data:
				// let source = { uri: 'data:image/jpeg;base64,' + response.data };
				this.setState({
					update: true,
					changeAvatar: true,
					avatar: response.uri,
				});
			}
		});
	}
	componentDidMount() {
		for (let student of Data_Student) {
			if (student.studentID == global.loginName) {
				this.setState({
					avatar: 'http://file.vforum.vn/hinh/2018/03/hinh-anh-hinh-nen-zoro-dep-nhat-one-piece-4.jpg',
					studentID: student.studentID,
					fullName: student.fullName,
					sex: student.sex,
					birthday: student.birthday,
					email: student.email,
					address: student.address,
					phoneNumber: student.phoneNumber,
					unit: student.unit,
					intro: student.intro,
					update: false,
				});
			}
		}
	}

	render() {
		changeFullName = () => {
			this.setState({
				changeFullName: true,
				changeAvatar: false,
				changeSex: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
				update: true,
			});
		};
		handleChangeFullName = fullName => {
			this.setState({ fullName: fullName });
		};

		changeSex = () => {
			this.setState({
				changeSex: true,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
			});
		};
		handleChangeSex = sex => {
			this.setState({ sex: sex });
		};

		changeBirthday = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: true,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
			});
		};
		handleChangeBirthday = birthday => {
			this.setState({ birthday: birthday });
		};

		changeEmail = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: true,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
			});
		};
		handleChangeEmail = email => {
			this.setState({ email: email });
		};

		changeAddress = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: true,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
			});
		};
		handleChangeAddress = address => {
			this.setState({ address: address });
		};

		changePhoneNumber = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: true,
				changeUnit: false,
				changeIntro: false,
			});
		};
		handleChangePhoneNumber = phoneNumber => {
			this.setState({ phoneNumber: phoneNumber });
		};

		changeUnit = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: true,
				changeIntro: false,
			});
		};
		handleChangeUnit = unit => {
			this.setState({ unit: unit });
		};

		changeIntro = () => {
			this.setState({
				changeSex: false,
				update: true,
				changeAvatar: false,
				changeFullName: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: true,
			});
		};
		handleChangeIntro = intro => {
			this.setState({ intro: intro });
		};
		save = () => {
			this.setState({
				update: false,
				changeAvatar: false,
				changeFullName: false,
				changeSex: false,
				changeBirthday: false,
				changeEmail: false,
				changeAddress: false,
				changePhoneNumber: false,
				changeUnit: false,
				changeIntro: false,
			});
		};

		return (
			<View style={styles.container}>
				<ScrollView style={styles.container}>
					<View style={styles.avatarContainer}>
						<TouchableOpacity style={styles.contentContainer} onPress={this.selectPhotoTapped.bind(this)}>
							<Text style={styles.titleContent}>Hình ảnh đại diện:</Text>
							<View>
								{this.state.avatar === null ? (
									<Image
										style={{ width: 50, height: 50, borderRadius: 100 }}
										source={{
											uri: this.state.avatar,
										}}
									/>
								) : (
									<Image
										style={{ width: 50, height: 50, borderRadius: 100 }}
										source={{
											uri: this.state.avatar,
										}}
									/>
								)}
							</View>
						</TouchableOpacity>
						<View style={styles.contentContainer}>
							<Text style={styles.titleContent}>Tên đăng nhập:</Text>
							<Text style={styles.content}> {this.state.studentID}</Text>
						</View>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Họ và tên:</Text>
						{this.state.changeFullName === true ? (
							<TextInput
								autoFocus={this.state.changeFullName}
								style={styles.content}
								value={this.state.fullName}
								onChangeText={handleChangeFullName}
							/>
						) : (
							<Text style={styles.content}> {this.state.fullName}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeFullName}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Giới tính:</Text>
						{this.state.changeSex === true ? (
							<View style={{ flex: 1 }}>
								<Picker
									style={styles.content}
									selectedValue={this.state.sex}
									onValueChange={handleChangeSex}
								>
									<Picker.Item label="Nam" value="Nam" />
									<Picker.Item label="Nữ" value="Nữ" />
								</Picker>
							</View>
						) : (
							<Text style={styles.content}> {this.state.sex}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeSex}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Ngày sinh:</Text>
						{this.state.changeBirthday === true ? (
							<DatePicker
								date={this.state.birthday}
								mode="date"
								format="DD-MM-YYYY"
								minDate="01-01-1900"
								maxDate="31-12-2019"
								showIcon={false}
								onDateChange={handleChangeBirthday}
							/>
						) : (
							<Text style={styles.content}> {this.state.birthday}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeBirthday}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Email:</Text>
						{this.state.changeEmail === true ? (
							<TextInput
								autoFocus={this.state.changeEmail}
								style={styles.content}
								value={this.state.email}
								onChangeText={handleChangeEmail}
								keyboardType={'email-address'}
							/>
						) : (
							<Text style={styles.content}> {this.state.email}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeEmail}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Địa chỉ:</Text>
						{this.state.changeAddress === true ? (
							<View style={{ flex: 1 }}>
								<TextInput
									autoFocus={this.state.changeAddress}
									style={styles.content}
									value={this.state.address}
									onChangeText={handleChangeAddress}
								/>
							</View>
						) : (
							<Text style={styles.content}> {this.state.address}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeAddress}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Số điện thoại:</Text>
						{this.state.changePhoneNumber === true ? (
							<TextInput
								keyboardType={'numeric'}
								autoFocus={this.state.changePhoneNumber}
								style={styles.content}
								value={this.state.phoneNumber}
								onChangeText={handleChangePhoneNumber}
							/>
						) : (
							<Text style={styles.content}> {this.state.phoneNumber}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changePhoneNumber}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.contentContainer}>
						<Text style={styles.titleContent}>Đơn vị:</Text>
						{this.state.changeUnit === true ? (
							<TextInput
								autoFocus={this.state.changeUnit}
								style={styles.content}
								value={this.state.unit}
								onChangeText={handleChangeUnit}
							/>
						) : (
							<Text style={styles.content}> {this.state.unit}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeUnit}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
					<View style={styles.introContainer}>
						<Text style={styles.titleContent}>Giới thiệu bản thân:</Text>
						{this.state.changeIntro === true ? (
							<TextInput
								autoFocus={this.state.changeIntro}
								style={styles.content}
								multiline={true}
								numberOfLines={4}
								value={this.state.intro}
								onChangeText={handleChangeIntro}
							/>
						) : (
							<Text style={styles.content}> {this.state.intro}</Text>
						)}
						<TouchableOpacity style={styles.icon} onPress={changeIntro}>
							<IconAns5 name="edit" color="#993399" size={18} />
						</TouchableOpacity>
					</View>
				</ScrollView>
				{this.state.update === true ? (
					<TouchableOpacity style={styles.footer} onPress={save}>
						<Text style={styles.footerTitle}>Cập nhật</Text>
					</TouchableOpacity>
				) : (
					<View />
				)}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'whitesmoke',
	},
	avatarContainer: {
		marginVertical: 10,
		backgroundColor: 'white',
	},
	contentContainer: {
		backgroundColor: 'white',
		padding: 5,
		paddingHorizontal: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: 69,
		borderBottomWidth: 2,
		borderBottomColor: 'whitesmoke',
	},
	introContainer: {
		backgroundColor: 'white',
		padding: 5,
		paddingHorizontal: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		minHeight: 69,
		borderBottomWidth: 2,
		borderBottomColor: 'whitesmoke',
	},
	icon: {
		// flex: 1 ,
		alignItems: 'center',
		justifyContent: 'center',
	},
	titleContent: {
		width: 120,
		alignItems: 'flex-start',
		fontSize: 14,
		fontWeight: 'bold',
	},
	content: {
		flex: 1,
		alignItems: 'flex-start',
	},
	footer: {
		backgroundColor: '#673AB7',
		alignItems: 'center',
		justifyContent: 'center',
		height: 40,
	},
	footerTitle: {
		alignItems: 'center',
		fontSize: 18,
		color: 'white',
	},
});
export default StudentProfile;
