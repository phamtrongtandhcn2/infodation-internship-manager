import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Data_Timeline from '../datas/data_timeline';
import ShortTitle from '../function/shortTitle';
export default class StudentInternshipDetail extends React.Component {
	constructor() {
		super();
		this.data = Data_Timeline;
		this.state = {
			key: '',
			time: '',
			title: '',
			decription: '',
		};
	}
	componentDidMount() {
		for (var i = 0; i < Data_Timeline.length; i++) {
			var timeline = Data_Timeline[i];
			if (timeline.key === global.timeline) {
				this.setState({
					key: timeline.key,
					time: timeline.time,
					title: timeline.title,
					decription: timeline.description,
				});
			}
		}
	}
	render() {
		return (
			<View style={{ flex: 1, backgroundColor: 'lavender' }}>
				<View style={styles.header}>
					<TouchableOpacity
						onPress={() => {
							this.props.navigation.goBack();
						}}
					>
						<Icon name="chevron-left" color="white" size={35} />
					</TouchableOpacity>
					{/* Short title  */}
					<Text style={styles.titleHeader}>
						{this.state.time}: {ShortTitle(this.state.title)}
					</Text>
				</View>
				<ScrollView style={{ flex: 1, backgroundColor: 'white', marginVertical: 15, margin: 10 }}>
					<View style={{ flex: 1, margin: 10 }}>
						<Text style={{ fontSize: 20 }}>{this.state.title}</Text>
						<Text style={{ marginBottom: 20, fontSize: 13, fontStyle: 'italic' }}>{this.state.time}</Text>
						<Text style={{ fontSize: 16 }}>{this.state.decription}</Text>
					</View>

					<View
						style={{
							margin:10,
							height: 50,
							borderWidth: 1,
							borderColor: '#993399',
							flexDirection: 'row',
							alignItems: 'center',
							backgroundColor: 'white',
						}}
					>
						<TextInput style={{ flex: 1, padding: 5 }} 
						placeholder={'Nhập nội dung'}
						multiline = {true} 
						/>
						<View style={{ flex: 1 / 3 }}>
							<TouchableOpacity
								style={{
									flex: 1,
									backgroundColor: '#993399',
									alignItems: 'center',
									justifyContent: 'center',
								}}
							>
								<Text style={{ color: 'white' }}>Hoàn tất</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	header: {
		height: 60,
		flexDirection: 'row',
		borderBottomColor: 'purple',
		borderBottomWidth: 2,
		alignItems: 'center',
		paddingHorizontal: 2,
		backgroundColor: '#993399',
	},
	titleHeader: {
		color: 'white',
		fontSize: 18,
		marginHorizontal: 5,
	},
});
