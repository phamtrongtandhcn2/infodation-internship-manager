import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';

export default class SignUpScreen extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.headerTitle}>ĐĂNG KÝ THỰC TẬP TẠI {'\n'} CÔNG TY INFODATION</Text>
				</View>
				<ScrollView style={styles.body}>
					<View>
						<Text>{INFODATION}</Text>
					</View>
				</ScrollView>
				<TouchableOpacity style={styles.footer}>
					<Text style={styles.footerTitle}>Đăng ký ngay!</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		backgroundColor: '#673AB7',
		alignItems: 'center',
		justifyContent: 'center',
		height: 60,
	},
	body: {
		margin: 10,
	},
	footer: {
		backgroundColor: '#673AB7',
		alignItems: 'center',
		justifyContent: 'center',
		height: 40,
	},
	headerTitle: {
		alignItems: 'center',
		fontSize: 22,
		color: 'white',
	},
	footerTitle: {
		alignItems: 'center',
		fontSize: 18,
		color: 'white',
	},
});
var INFODATION = `  Công ty INFOdation là công ty cung cấp các giải pháp phần mềm cho khách hàng và chuyển đổi ý tưởng kinh doanh của khách hàng thành hiện thực.
    
    INFOdation Co., Ltd (IFD), thành lập năm 2008 bởi Jan Bouw, Chi Mai và Gerrit Mulder, với văn phòng chính tại Bodegraven (Hà Lan) và chi nhánh tại Nha Trang (Việt Nam).
    
    IFD mang đến cho nhân viên một chế độ phúc lợi và môi trường làm việc đáp ứng hầu hết các nhu cầu cho nhân viên:
-   Lương thưởng cạnh tranh theo năng lực
-   Cơ hội thăng tiến rộng mở
-   Cơ hội đi công tác tại Hà Lan
-   Các chương trình đào tạo xuất sắc
-   Đánh giá nhân viên công bằng
-   Hoạt động teambuilding, nghỉ mát hàng năm
-   Được khám sức khỏe tổng quát tại bệnh viện Quốc Tế Vinmec Nha Trang
-   BHXH, BHYT & BH thất nghiệp đầy đủ
-   Môi trường làm việc tiện nghi, thoải mái, có góc ăn uống và giải trí riêng
-   Đội ngũ nhân viên năng động, làm việc với tinh thần đồng đội cao
Địa chỉ: Tầng 10, Tòa nhà VNPT, 50 Lê Thánh Tôn, Tp. Nha Trang, tỉnh Khánh Hòa
Điện thoại: 84-258-3526477 
E-mail: info@infodation.nl 
Website:    http://www.infodation.nl 
	hoặc 
	https://www.facebook.com/infodation/ 
    
    Hiện tại, công ty INFOdation cần tuyển vị trí sinh viên thực tập:
    1. .NET Developer
    2. Front End Developer (HTML, JavaScript, CSS, PHP)
    3. Business Analyst
    4. Tester
    5. JAVA Developer
    6. Hybrid Mobile Developer
    7. Microsoft Systems Engineer

    Sinh viên thực tập tốt được giữ lại làm việc chính thức tại công ty sau thời gian thực tập.`;
