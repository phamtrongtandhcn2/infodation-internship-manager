import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import StudentHeader from '../components/studentHeader';
import IconAns5 from 'react-native-vector-icons/FontAwesome5';
class StudentRealityProject extends Component {
	static navigationOptions = {
		drawerLabel: 'Reality Project',
		drawerIcon: () => <IconAns5 name="file-code" color="#993399" size={18} />,
	}
	render() {
		return (
			<View style={{ flex: 1 }}>
				<StudentHeader titleHeader={'Reality Project'} navigation = {this.props.navigation} />
			</View>
		);
	}
}

export default StudentRealityProject;
