import SplashScreen from '../screens/splashScreen';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import { LoginRouter } from '../routers/loginRouter';
import { StudentRouter } from '../routers/studentRouter';
import { ManagerRouter } from '../routers/managerRouter';
import { StudentInternRouter } from '../routers/studentInternRouter';

export const Router = createAppContainer(
	createSwitchNavigator(
		{
			SplashScreen: SplashScreen,
			LoginRouter: LoginRouter,
			StudentRouter: StudentRouter,
			StudentInternRouter: StudentInternRouter,
			ManagerRouter: ManagerRouter
		},
		{
			initialRouteName: 'SplashScreen',
		}
	)
);
