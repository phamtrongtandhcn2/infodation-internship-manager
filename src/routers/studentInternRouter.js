import StudentInternshipContent from '../screens/studentInternshipContent';
import StudentInternshipDetail from '../screens/studentInternshipDetail';
import { createStackNavigator } from 'react-navigation';
export const StudentInternRouter = createStackNavigator(
	{
		StudentInternshipContent: {
			screen: StudentInternshipContent,
			navigationOptions: {
				header: null,
			},
		},
		StudentInternshipDetail: {
			screen: StudentInternshipDetail,
			navigationOptions: {
				header: null,
			},
		},
	},
	{
		initialRouteName: 'StudentInternshipContent',
	}
);
