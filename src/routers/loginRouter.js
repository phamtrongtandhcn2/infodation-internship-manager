import LoginScreen from '../screens/loginScreen';
import SignUpScreen from '../screens/signUpScreen';
import { createStackNavigator } from 'react-navigation';
export const LoginRouter = createStackNavigator(
	{
		LoginScreen: {
			screen: LoginScreen,
			navigationOptions: {
				header: null,
			},
		},
		SignUpScreen: {
			screen: SignUpScreen,
			navigationOptions: {
				header: null,
			},
		},
	},
	{
		initialRouteName: 'LoginScreen',
	}
);
