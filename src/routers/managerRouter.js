import React from 'react';
import IconAns5 from 'react-native-vector-icons/FontAwesome5';
import ManagerProfile from '../screens/managerProfile';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import ManagerDrawerContent from '../components/managerDrawerContent';
export const ManagerRouter = createAppContainer(
	createDrawerNavigator(
		{
			Profile: {
				screen: ManagerProfile,
			},
		},
		{
			// initialRouteName: 'Internship',
			initialRouteName: 'Profile',
			contentComponent: ManagerDrawerContent,
		}
	)
);
