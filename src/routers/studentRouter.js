import React from 'react';
import IconAns5 from 'react-native-vector-icons/FontAwesome5';
import StudentProfile from '../screens/studentProfile';
import StudentProjectIntern from '../screens/studentProjectIntern';
import StudentRealityProject from '../screens/studentRealityProject';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import StudentDrawerContent from '../components/studentDrawerContent';
import { StudentInternRouter } from '../routers/studentInternRouter';
export const StudentRouter = createAppContainer(
	createDrawerNavigator(
		{
			Profile: {
				screen: StudentProfile,
			},
			Internship: {
				screen: StudentInternRouter,
				navigationOptions: {
					drawerLabel: 'Internship',
					drawerIcon: () => <IconAns5 name="book" color="#993399" size={18} />,
				},
			},
			ProjectIntert: {
				screen: StudentProjectIntern,
			},
			RealityProject: {
				screen: StudentRealityProject,
			},
		},
		{
			initialRouteName: 'Internship',
			// initialRouteName: 'Profile',
			contentComponent: StudentDrawerContent,
		}
	)
);
